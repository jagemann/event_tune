
## Api
 - `/api/event/create` (POST) either json or formdata
 - `/api/event/search` (GET)
	Query Params:
	- name
	- tag
	- date_range
	- create_date
	- event_date
	- event_type

## Servics
 - Weather Service (or via js in frontend)
 - Auto Populator

## DB-Layer
 - GetEventData (id) -> EventDTO
 - GetEventList () -> [EventShortDTO]
 - GetEventSearch (...params) -> [EventShortDTO]
 - GetHotEvents () [EventShortDTO][3]
 - CreateEvent (...data) -> void

## DB
 - Event
   - Title
   - (Short_Description => first 100 chars of Desc.)
   - Tags[]
   - Type
   - Description
   - CreationDate
   - EventDate
   - Latitude/Longitude or City
 - Vote
   - Event (ForeignKey)
   - UserSessionId (WeakId)
   - UpDownVoteValue => -1/0/+1
   
## Objects (DTO)
 - EventShortDTO
   - Title
   - Short_Description
   - Tags[]
   - Type
 - EventDTO
   - \+ EventShortDTO
   - Full_Description
   - CreationDate
   - EventDate
   - Latitude/Longitude or City
 - VoteDTO
   - Event (ForeignKey)
   - UserSessionId (WeakId)
   - UpDownVoteValue 
