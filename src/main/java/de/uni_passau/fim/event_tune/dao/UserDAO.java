package de.uni_passau.fim.event_tune.dao;

import de.uni_passau.fim.event_tune.dto.UserDTO;
import de.uni_passau.fim.event_tune.util.HibernateUtil;
import org.hibernate.SessionFactory;

public class UserDAO {
    // TODO autowired?
    private static SessionFactory sf = HibernateUtil.getSessionFactory();

    public static Integer getNewUser() {
        var session = sf.openSession();
        session.beginTransaction();
        var uid = (Integer)session.save(new UserDTO());
        session.getTransaction().commit();
        session.close();
        return uid;
    }
}
