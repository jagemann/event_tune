package de.uni_passau.fim.event_tune.dao;

import de.uni_passau.fim.event_tune.data.SqlResult;
import de.uni_passau.fim.event_tune.dto.EventDTO;
import de.uni_passau.fim.event_tune.dto.UserVoteDTO;
import de.uni_passau.fim.event_tune.dto.VoteId;
import de.uni_passau.fim.event_tune.dto.VoteSumDTO;
import de.uni_passau.fim.event_tune.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;

import java.util.*;

// EventDAO handles both Full and Short EventDTOs
public final class EventDAO {
    // TODO autowired?
    private static SessionFactory sf = HibernateUtil.getSessionFactory();

    public static SqlResult tryAdd(EventDTO event) {
        var session = sf.openSession();
        session.beginTransaction();
        event.setCreationDate(new Date());
        try {
            session.save(event);
        } catch (ConstraintViolationException e) {
            return SqlResult.DuplicateEntry;
        }
        session.save(new UserVoteDTO(new VoteId(event.getId(), 0), 0));
        session.getTransaction().commit();
        session.close();
        return SqlResult.Ok;
    }

    public static List<EventDTO> getLastAdded(int max) {
        var session = sf.openSession();
        List<EventDTO> resEvents = session.createQuery("from Events e order by e.creationDate").setMaxResults(Math.min(max, 20)).list();
        session.close();
        return resEvents;
    }

    public static EventDTO getById(int id) {
        var session = sf.openSession();
        var res = (EventDTO)session.createQuery("from Events e where e.id = :id").setInteger("id", id).uniqueResult();
        return res;
    }

    /**
     * Gets the hottest (by up/downvoted ratio) events which are not in the past
     */
    public static List<EventDTO> getHot(int max) {
        var session = sf.openSession();
        // Yeah, this could probably be be solved with a hql join, but I don't think this is possible if we didn't
        // mark the two tables related in the Entity declaration and I'm too lazy to fix it.
        List<Integer> evIds = (List<Integer>)session.createQuery("select v.id.eventId from Votes v group by v.id.eventId order by sum(v.vote) desc").list();
        System.out.println(evIds);
        List<EventDTO> evs = (List<EventDTO>)session.createQuery("from Events e where e.date > :now and e.id in :ids").setDate("now", new Date()).setParameterList("ids", evIds).setMaxResults(Math.min(max, 3)).list();
        evs.sort(Comparator.comparingInt((EventDTO e) -> evIds.indexOf(e.getId())));
        return evs;
    }

    public static List<EventDTO> search(Optional<String> title) {
        return new ArrayList<>(); // TODO
    }

    public static VoteSumDTO setVote(int eventId, int userId, int vote) {
        var session = sf.openSession();
        session.beginTransaction();
        var voteDto = new UserVoteDTO();
        voteDto.setId(new VoteId(eventId, userId));
        voteDto.setVote(vote);
        session.saveOrUpdate(voteDto);
        var res = getVotesFromSessionInternal(session, eventId);
        session.getTransaction().commit();
        session.close();
        return res;
    }

    public static int getUserVote(int eventId, int userId) {
        var session = sf.openSession();
        UserVoteDTO vote = (UserVoteDTO)session.createQuery("from Votes v where v.id.eventId = :eventId and v.id.userId = :userId")
                .setInteger("eventId", eventId)
                .setInteger("userId", userId).uniqueResult();
        session.close();
        if (vote == null) return 0;
        return vote.getVote();
    }

    public static VoteSumDTO getEventVotes(int eventId) {
        var session = sf.openSession();
        session.beginTransaction();
        var res = getVotesFromSessionInternal(session, eventId);
        session.getTransaction().commit();
        session.close();
        return res;
    }

    private static VoteSumDTO getVotesFromSessionInternal(Session session, int eventId)
    {
        Long up = (Long) session.createQuery("select sum(v.vote) from Votes v where v.id.eventId = :eventId and v.vote > 0").setInteger("eventId", eventId).uniqueResult();
        Long down = (Long) session.createQuery("select sum(v.vote) from Votes v where v.id.eventId = :eventId and v.vote < 0").setInteger("eventId", eventId).uniqueResult();
        if (up == null) up = 0L;
        if (down == null) down = 0L;
        return new VoteSumDTO(up.intValue(), Math.abs(down.intValue()));
    }
}
