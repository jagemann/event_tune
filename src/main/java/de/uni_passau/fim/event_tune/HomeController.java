package de.uni_passau.fim.event_tune;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class HomeController {

    /**
     * Just a dummy forwarder to our main event view.
     * This is just here in case we want to add a useful '/' landing page in the future;
     * and keep the paths clean for features like /user/login etc.
     */
    @RequestMapping("/")
    public RedirectView defaultRedirect(RedirectAttributes attributes) {
        //attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
        //attributes.addAttribute("attribute", "redirectWithRedirectView");
        return new RedirectView("event/list");
    }
}
