package de.uni_passau.fim.event_tune;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventTuneApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventTuneApplication.class, args);
    }
}
