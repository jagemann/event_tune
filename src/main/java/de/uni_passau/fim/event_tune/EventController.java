package de.uni_passau.fim.event_tune;

import de.uni_passau.fim.event_tune.dao.EventDAO;
import de.uni_passau.fim.event_tune.data.SqlResult;
import de.uni_passau.fim.event_tune.dto.EventDTO;
import de.uni_passau.fim.event_tune.dto.VoteSumDTO;
import de.uni_passau.fim.event_tune.util.HibernateUtil;
import de.uni_passau.fim.event_tune.util.UserVoteSession;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;

@Controller
@RequestMapping("/event")
public class EventController {
    // TODO autowired?
    private SessionFactory sf = HibernateUtil.getSessionFactory();
    private static final DateFormat datePostFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final EventDTO notExistingEventDummy = createDummy();
    private static final VoteSumDTO notExistingVoteSumDummy = new VoteSumDTO(-1, -1);

    public EventController() {
        System.out.println("Created ev controller");
    }

    private static EventDTO createDummy() {
        var ev = new EventDTO();
        ev.setTitle("THIS EVENT DOES NOT EXIST");
        return ev;
    }

    @RequestMapping("/list")
    public String list(Model model, HttpServletRequest request) {
        return "event/list";
    }

    @RequestMapping("/view/{eventId}")
    public String view(Model model, HttpServletRequest request, @PathVariable int eventId) {
        var event = EventDAO.getById(eventId);
        VoteSumDTO currentVote;
        int ownVote;
        if (event == null) {
            // TODO alternatively redirect to 404 ?
            event = notExistingEventDummy;
            currentVote = notExistingVoteSumDummy;
            ownVote = -1;
        } else {
            currentVote = EventDAO.getEventVotes(eventId);
            var session = UserVoteSession.getUserVoteSession(request);
            if (session.hasId()) {
                var userId = session.getOrCreateId();
                ownVote = EventDAO.getUserVote(eventId, userId);
            } else {
                ownVote = 0;
            }
        }

        model.addAttribute("current_vote", currentVote);
        model.addAttribute("own_vote", ownVote);
        model.addAttribute("event_id", eventId);
        model.addAttribute("ev", event);
        return "event/view";
    }

    @RequestMapping("/form")
    public String form(Model model, HttpServletRequest request) {
        model.addAttribute("ev", new EventDTO());
        return "event/form";
    }

    @PostMapping("/create_post")
    public String create(@RequestParam String title,
                         @RequestParam String description,
                         @RequestParam String date,
                         @RequestParam String city,
                         @RequestParam String kind,
                         Model model,
                         HttpServletResponse response) {
        boolean isError = false;
        var eventDto = new EventDTO();
        eventDto.setTitle(title);
        eventDto.setDescription(description);
        try {
            eventDto.setDate(datePostFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            isError = true;
        }
        // TODO check if city actually exists
        eventDto.setCity(city);
        eventDto.setKind(kind);

        SqlResult result = SqlResult.Unknown;
        if (!isError) {
            result = EventDAO.tryAdd(eventDto);
        }
        if (!isError && result == SqlResult.Ok) {
            return "redirect:/event/view/" + eventDto.getId();
        } else {
            if (result == SqlResult.DuplicateEntry) {
                model.addAttribute("err_title", "This title is already taken");
            }
            model.addAttribute("ev", eventDto);
            return "event/form";
        }
    }
}
