package de.uni_passau.fim.event_tune.data;

// I wish i had rust with union types here...
public enum SqlResult {
    Ok,
    Unknown,
    InvalidField,
    DuplicateEntry,
}
