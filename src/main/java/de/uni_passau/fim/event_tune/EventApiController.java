package de.uni_passau.fim.event_tune;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_passau.fim.event_tune.dao.EventDAO;
import de.uni_passau.fim.event_tune.data.SqlResult;
import de.uni_passau.fim.event_tune.dto.EventDTO;
import de.uni_passau.fim.event_tune.dto.VoteSumDTO;
import de.uni_passau.fim.event_tune.util.*;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api/event")
public class EventApiController {

    public EventApiController() {
    }

    @PostMapping("/create")
    public SqlResult create(@RequestBody EventDTO event, HttpServletResponse response) {
        System.out.println(event);
        var result = EventDAO.tryAdd(event);
        if (result != SqlResult.Ok) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
        }
        return result;
    }

    /**
     * Returns the last 20 entries
     */
    @GetMapping(value = "/list")
    public List<EventDTO> list() {
        return EventDAO.getLastAdded(20);
    }

    @GetMapping(value = "/hot")
    public List<EventDTO> hot() {
        return EventDAO.getHot(3);
    }

    @GetMapping(value = "/view/{eventId}")
    public EventDTO view(@PathVariable int eventId) {
        return EventDAO.getById(eventId);
    }

    @GetMapping(value = "/weather/{eventId}")
    public MetaWeatherDataDay viewWeather(@PathVariable int eventId) {
        try {
            var event = EventDAO.getById(eventId);
            if (event == null)
                return null;

            var joEventDate = new DateTime(event.getDate()).withTimeAtStartOfDay();
            var joNow = DateTime.now().withTimeAtStartOfDay();
            var daysTillEvent = Days.daysBetween(joNow, joEventDate).getDays();
            if (daysTillEvent >= 7 || daysTillEvent < 0) {
                return null;
            }
            System.out.println("Days to event " + daysTillEvent);

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            var cityData = mapper.readValue(
                    new URL("https://www.metaweather.com/api/location/search/?query=" + event.getCity()),
                    MetaWeatherSearchEntry[].class);
            if (cityData.length == 0)
                return null;
            var cityWoeid = cityData[0].getWoeid();
            System.out.println(cityWoeid);

            var city_weather = mapper.readValue(
                    new URL("https://www.metaweather.com/api/location/" + cityWoeid),
                    MetaWeatherData.class);

            var wDays = city_weather.getConsolidated_weather();
            if (wDays.length < daysTillEvent) {
                return null;
            }

            return wDays[daysTillEvent];
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping(value = "/weather_search/{loc}")
    public MetaWeatherSearchEntry[] searchCity(@PathVariable String loc) {
        try {
            URL url = new URL("https://www.metaweather.com/api/location/search/?query=" + loc);
            ObjectMapper mapper = new ObjectMapper();
            var mws = mapper.readValue(url, MetaWeatherSearchEntry[].class);
            return mws;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping(value = "/vote/{eventId}/up")
    public VoteSumDTO voteUp(HttpServletRequest request, @PathVariable int eventId) {
        return voteInternal(request, eventId, +1);
    }

    @PostMapping(value = "/vote/{eventId}/down")
    public VoteSumDTO voteDown(HttpServletRequest request, @PathVariable int eventId) {
        return voteInternal(request, eventId, -1);
    }

    @PostMapping(value = "/gen_dummy/{count}")
    public void fillDummies(@PathVariable int count) {
        for (int i = 0; i < count; i++) {
            var ev = new EventDTO();
            ev.setTitle("Titllo" + new Random().nextInt());
            ev.setDescription("desc" + new Random().nextInt());
            ev.setCity("cittyy" + new Random().nextInt());
            ev.setDate(new Date());
            ev.setKind("kindo" + new Random().nextInt());
            EventDAO.tryAdd(ev);
        }
    }

    private VoteSumDTO voteInternal(HttpServletRequest request, int eventId, int voteWeight) {
        var session = UserVoteSession.getUserVoteSession(request);
        var userId = session.getOrCreateId();
        System.out.println("User " + userId + " voted with " + voteWeight);
        var currentVotes = EventDAO.setVote(eventId, userId, voteWeight);
        return currentVotes;
    }

    @GetMapping("/search")
    public void search(
            @RequestParam String title,
            @RequestParam Date afterDate,
            @RequestParam Date beforeDate,
            @RequestParam String tag,
            @RequestParam String place, // lat,long or city ?
            @RequestParam String type) {
        // return JSON:
        // [
        //     { <EventObject> }
        // ]
        // limit: 10 ?
    }
}
