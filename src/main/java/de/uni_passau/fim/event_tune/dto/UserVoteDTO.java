package de.uni_passau.fim.event_tune.dto;

// up/down-votes could be put into a separate data table
// advantage:
// - race-condition safe when accessing event object
// - easy tracking and updating of user voting
// disadvantage:
// - probably a bit slower running SQL SUM(*) for each request (maybe create a cached view in the db?)

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

// (event-id, user-session-id, vote: -1/0/+1)
@Entity(name = "Votes")
public class UserVoteDTO {
    @EmbeddedId
    private VoteId id;
    @Column(name = "vote", nullable = false)
    private int vote; // -1 | 0 | +1

    public UserVoteDTO() {
    }

    public UserVoteDTO(VoteId id, int vote) {
        this.id = id;
        this.vote = vote;
    }

    public VoteId getId() {
        return id;
    }

    public void setId(VoteId id) {
        this.id = id;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }
}
