package de.uni_passau.fim.event_tune.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class VoteId implements Serializable {

    @Column(name = "eventId")
    private int eventId;

    @Column(name = "userId")
    private int userId;

    public VoteId() {
    }

    public VoteId(int eventId, int userId) {
        this.eventId = eventId;
        this.userId = userId;
    }
}