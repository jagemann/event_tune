package de.uni_passau.fim.event_tune.dto;

public class VoteSumDTO {
    private int up;
    private int down;

    public VoteSumDTO() {}

    public VoteSumDTO(int up, int down) {
        this.up = up;
        this.down = down;
    }

    public int getUp() {
        return up;
    }

    public void setUp(int up) {
        this.up = up;
    }

    public int getDown() {
        return down;
    }

    public void setDown(int down) {
        this.down = down;
    }
}
