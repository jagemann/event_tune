package de.uni_passau.fim.event_tune.util;

import de.uni_passau.fim.event_tune.dao.UserDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserVoteSession {
    public static final String HAS_DB_ID = "hasDbId";
    public static final String UNIQUE_USER_ID = "uniqueId";

    private HttpSession session;

    public static UserVoteSession getUserVoteSession(HttpServletRequest request) {
        var session = request.getSession(false);
        if (session == null) {
            session = request.getSession(true);
            session.setAttribute(HAS_DB_ID, false);
            session.setAttribute(UNIQUE_USER_ID, 0);
            // Thank you Java for those beautiful time annotations, `int interval` is so useful without reading the
            // documentation because accepting a `Duration` is too mainstream and readable. The span below is 30min btw.
            session.setMaxInactiveInterval(30 * 60);
        }
        return new UserVoteSession(session);
    }

    private UserVoteSession(HttpSession session) {
        this.session = session;
    }

    public boolean hasId() {
        return (Boolean) session.getAttribute(HAS_DB_ID);
    }

    public int getOrCreateId() {
        if (!hasId()) {
            var userIdResult = UserDAO.getNewUser();
            session.setAttribute(HAS_DB_ID, true);
            session.setAttribute(UNIQUE_USER_ID, userIdResult);
            return userIdResult;
        } else {
            return (Integer) session.getAttribute(UNIQUE_USER_ID);
        }
    }
}
