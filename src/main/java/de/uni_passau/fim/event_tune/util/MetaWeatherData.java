package de.uni_passau.fim.event_tune.util;

public class MetaWeatherData {
    private MetaWeatherDataDay[] consolidated_weather;

    public MetaWeatherDataDay[] getConsolidated_weather() {
        return consolidated_weather;
    }

    public void setConsolidated_weather(MetaWeatherDataDay[] consolidated_weather) {
        this.consolidated_weather = consolidated_weather;
    }
}
